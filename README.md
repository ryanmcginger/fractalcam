# fractalcam

Fractals with python and USB webcams!

## Install

Install via pip:

```bash
pip install fractalcam
```

And then, use the command-line interface:

```bash
fracam --help
```
